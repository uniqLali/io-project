package locations;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;
import static org.junit.Assert.*;

public class BudynekTest {

    @Test
    public void obliczMocOświetlenia() {
        Budynek testBudynek = new Budynek(23);
        Poziom testPoziom1 = new Poziom(33);
        testPoziom1.dodajPomieszczenie(new Pomieszczenie(1, 10, 10, 10, 10));
        testBudynek.dodajPoziom(testPoziom1);
        assertEquals(1.0, testBudynek.obliczMocOświetlenia(), 0.0);

        Poziom testPoziom2 = new Poziom(34);
        testPoziom2.dodajPomieszczenie(new Pomieszczenie(2, 10, 10, 10, 10));
        testBudynek.dodajPoziom(testPoziom2);
        assertEquals(1.0, testBudynek.obliczMocOświetlenia(), 0.0);

        Poziom testPoziom3 = new Poziom(35);
        testPoziom3.dodajPomieszczenie(new Pomieszczenie(3, 10, 10, 10, 10));
        testBudynek.dodajPoziom(testPoziom3);
        assertEquals(1.0, testBudynek.obliczMocOświetlenia(), 0.0);
    }

    @Test
    public void obliczPowierzchnię() {
        Budynek testBudynek = new Budynek(23);
        Poziom testPoziom1 = new Poziom(33);
        testPoziom1.dodajPomieszczenie(new Pomieszczenie(1, 10, 10, 10, 10));
        testBudynek.dodajPoziom(testPoziom1);
        assertEquals(10.0, testBudynek.obliczPowierzchnię(), 0.0);

        Poziom testPoziom2 = new Poziom(34);
        testPoziom2.dodajPomieszczenie(new Pomieszczenie(2, 10, 10, 10, 10));
        testBudynek.dodajPoziom(testPoziom2);
        assertEquals(20.0, testBudynek.obliczPowierzchnię(), 0.0);

        Poziom testPoziom3 = new Poziom(35);
        testPoziom3.dodajPomieszczenie(new Pomieszczenie(3, 10, 10, 10, 10));
        testBudynek.dodajPoziom(testPoziom3);
        assertEquals(30.0, testBudynek.obliczPowierzchnię(), 0.0);
    }

    @Test
    public void obliczKubaturę() {
        Budynek testBudynek = new Budynek(23);
        Poziom testPoziom1 = new Poziom(33);
        testPoziom1.dodajPomieszczenie(new Pomieszczenie(1, 10, 10, 10, 10));
        testBudynek.dodajPoziom(testPoziom1);
        assertEquals(10.0, testBudynek.obliczKubaturę(), 0.0);

        Poziom testPoziom2 = new Poziom(34);
        testPoziom2.dodajPomieszczenie(new Pomieszczenie(2, 10, 10, 10, 10));
        testBudynek.dodajPoziom(testPoziom2);
        assertEquals(20.0, testBudynek.obliczKubaturę(), 0.0);

        Poziom testPoziom3 = new Poziom(35);
        testPoziom3.dodajPomieszczenie(new Pomieszczenie(3, 10, 10, 10, 10));
        testBudynek.dodajPoziom(testPoziom3);
        assertEquals(30.0, testBudynek.obliczKubaturę(), 0.0);
    }
}