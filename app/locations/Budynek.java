/**
 *
 */
package locations;

import java.util.ArrayList;

/**
 * @author Oskar
 */
public class Budynek extends Lokacja {

    private ArrayList<Poziom> poziomy;


    /**
     * @param id   - Unikalny identyfikator lokacji
     * @param name - Opcjonalna nazwa lokacji
     */
    public Budynek(int id, String name) {
        super(id, name);
        poziomy = new ArrayList<>();


    }

    /**
     * @param id - Unikalny identyfikator lokacji
     */
    public Budynek(int id) {
        super(id);
        poziomy = new ArrayList<>();
    }

    /**
     * Przypisuje poziom do danego budynku
     *
     * @param p - przypisywany poziom
     */
    public void dodajPoziom(Poziom p) {
        poziomy.add(p);
    }

    /**
     * Usuwa przypisanie poziomu do budynku
     *
     * @param p - poziom usuwany z budynku
     */
    public void usuńPoziom(Poziom p) {
        poziomy.remove(p);
    }

    /**
     * Wyliczane jako średnia moc na poziomach
     */
    @Override
    public double obliczMocOświetlenia() {
        double średniaMoc = 0;
        for (Poziom p : poziomy)
            średniaMoc += p.obliczMocOświetlenia();

        return średniaMoc / poziomy.size();
    }

    /**
     * Obliczanie całkowitej powierzchni budynku poprzez sumowanie całkowitej powierzchni poziomów.
     * @return - zwraca sumaryczną powierzchnię budynku.
     */
    @Override
    public double obliczPowierzchnię() {
        int sum_area=0;
        for(Poziom poz: poziomy){
            sum_area+=poz.obliczPowierzchnię();
        }
        return sum_area;
    }



    /**
     * @return Kubaturę budynku wyliczaną jako suma kubatury poziomów
     */
    @Override
    public double obliczKubaturę() {
        double kubaturaBudynku = 0;
        for (Poziom p : poziomy) {
            kubaturaBudynku += p.obliczKubaturę();
        }
        return kubaturaBudynku;
    }

    public ArrayList<Poziom> getPoziomy() {
        return poziomy;
    }

    public void setPoziomy(ArrayList<Poziom> poziomy) {
        this.poziomy = poziomy;
    }

}
