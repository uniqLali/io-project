package locations;

import java.util.HashSet;
import java.util.Set;

public class BazaBudynkow {

    public static Set<Budynek> budynekSet = new HashSet<Budynek>();

    public BazaBudynkow() {
        Poziom p0 = new Poziom(0, "parter");
        Poziom p1 = new Poziom(0, "pierwsze");
        Poziom p2 = new Poziom(0, "drugie");

        Pomieszczenie pom1 = new Pomieszczenie(1,"balkon",154,23,10,123);
        Pomieszczenie pom2 = new Pomieszczenie(1,"kuchnia",12,2,111,12);
        Pomieszczenie pom3 = new Pomieszczenie(1,"salon",1,2,1,1);

        p0.dodajPomieszczenie(pom1);
        p0.dodajPomieszczenie(pom3);
        p0.dodajPomieszczenie(pom2);
        p1.dodajPomieszczenie(pom3);
        p2.dodajPomieszczenie(pom2);

        Budynek b1 = new Budynek(1, "wieżowiec");
        b1.dodajPoziom(p0);
        b1.dodajPoziom(p1);
        b1.dodajPoziom(p2);
        Budynek b2 = new Budynek(2, "sky tower");
        b2.dodajPoziom(p0);
        b2.dodajPoziom(p1);

        Budynek b3 = new Budynek(3, "stary browar");
        b3.dodajPoziom(p0);

        this.budynekSet.add(b1);
        this.budynekSet.add(b2);
        this.budynekSet.add(b3);
    }

    public static Set<Budynek> getBudynekSet() {
        return budynekSet;
    }

    public static void setBudynekSet(Set<Budynek> budynekSet) {
        BazaBudynkow.budynekSet = budynekSet;
    }
}
