/**
 * 
 */
package locations;

import java.util.ArrayList;

/**
 * @author Oskar
 *
 */
public class Poziom extends Lokacja {

	private ArrayList<Pomieszczenie> pomieszczenia;
	
	/**
	 * @param id - Unikalny identyfikator lokacji
	 * @param name - Opcjonalna nazwa lokacji
	 */
	public Poziom(int id, String name) {
		super(id, name);
		pomieszczenia = new ArrayList<>();
	}

	/**
	 * @param id - Unikalny identyfikator lokacji
	 */
	public Poziom(int id) {
		super(id);
		pomieszczenia = new ArrayList<>();
	}

	/**
	 * Przypisuje pomieszczenie do danego poziomu
	 * @param p - przypisywane pomieszczenie
	 */
	public void dodajPomieszczenie(Pomieszczenie p) {
		pomieszczenia.add(p);
	}
	
	/**
	 * Usuwa przypisanie pomieszczenia do poziomu
	 * @param p - usuwane pomieszczenie
	 */
	public void usuńPomieszczenie(Pomieszczenie p) {
		pomieszczenia.remove(p);
	}
	
	/**
	 * @return Wyliczane jako średnia moc na m^2 na poziomie
	 */
	@Override
	public double obliczMocOświetlenia() {
		double moc =0;
		double powierzchnia =0;
		for(Pomieszczenie p : pomieszczenia) {
			moc+=p.obliczMocOświetlenia();
			powierzchnia+=p.obliczPowierzchnię();
		}
		return moc/powierzchnia;
	}

	/**
	 * Wyliczanie powierzchni poziomu poprzez sumowanie powierzchni pomieszczen.
	 * @return - zwraca sumaryczna powierzchnie poziomu.
	 */
	@Override
	public double obliczPowierzchnię() {
		int sum_area=0;
		for(Pomieszczenie pom: pomieszczenia){
			sum_area+=pom.obliczPowierzchnię();
		}
		return sum_area;
	}

    /**
     * @return Kubaturę poziomu wyliczaną jako suma kubatury pomieszczeń na poziomie
     */
	@Override
	public double obliczKubaturę() {
		double kubaturaPoziomu = 0;
		for (Pomieszczenie p : pomieszczenia) {
		    kubaturaPoziomu += p.obliczKubaturę();
        }
		return kubaturaPoziomu;
	}

}
