package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import locations.BazaBudynkow;
import locations.Budynek;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.createBuilding;
import views.html.infoView;

import javax.inject.Inject;

public class BuildingController extends Controller {

    @Inject
    FormFactory formFactory;


    public Budynek findBuildingById(int id) {
        BazaBudynkow bazaBudynkow = new BazaBudynkow();
        for (Budynek budynek : bazaBudynkow.getBudynekSet()) {
            if (budynek.getId() == id) {
                return budynek;
            }
        }
        return null;
    }

    public Result obliczOswietlenia(int idBudynku) {
        double mocOświetlenia = 0;
        Budynek budynek = findBuildingById(idBudynku);

        if (budynek == null) {
            return ok("nie ma takiego budynku");
        } else {
            mocOświetlenia = budynek.obliczMocOświetlenia();
            return ok(infoView.render("Moc oświetlenia", mocOświetlenia));
        }
    }

    public Result obliczPowierzchnie(int idBudynku) {
        double powierzchnia = 0;
        Budynek budynek = findBuildingById(idBudynku);

        if (budynek == null) {
            return ok("nie ma takiego budynku");
        } else {
            powierzchnia = budynek.obliczPowierzchnię();
            return ok(infoView.render("Powierzchnia budynku", powierzchnia));
        }
    }

    public Result obliczKubature(int idBudynku) {
        Budynek budynek = findBuildingById(idBudynku);
        if (budynek == null) {
            return ok("nie ma takiego budynku");
        } else {
            double kubatura = budynek.obliczKubaturę();
            return ok(infoView.render("Kubatura budynku", kubatura));
        }
    }


    public Result create() {
        Form<Budynek> buildingForm = formFactory.form(Budynek.class);
        return ok(createBuilding.render(buildingForm));
    }

    public Result writeInfoAbout(int id) {
        Budynek budynek = findBuildingById(id);
        if (budynek == null) {
            return ok("nie ma takiego budynku");
        }
        return ok(Json.toJson(budynek));
    }

    public Result writeInfoAboutAllBuildings() {
        BazaBudynkow bazaBudynkow = new BazaBudynkow();
        return ok(Json.toJson(bazaBudynkow.getBudynekSet()));
    }

    public Budynek getFromJson() {
        JsonNode json = Json.parse("{\"name\":\"Food\", \"age\":13}");
        Budynek budynek = Json.fromJson(json, Budynek.class);
        return budynek;
    }

}
